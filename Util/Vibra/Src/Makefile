# 
# Copyright (C) 1996-2016	The SIESTA group
#  This file is distributed under the terms of the
#  GNU General Public License: see COPYING in the top directory
#  or http://www.gnu.org/copyleft/gpl.txt.
# See Docs/Contributors.txt for a list of contributors.
#
# Makefile for Vibra package

.SUFFIXES:
.SUFFIXES: .f .F .o .a .f90 .F90

.PHONY: default dep clean

PROGS:= fcbuild vibra
default: $(PROGS)

override WITH_MPI=
override WITH_NETCDF=
override WITH_FLOOK=

MAIN_OBJDIR=.
TOPDIR=.

ARCH_MAKE=$(MAIN_OBJDIR)/arch.make
include $(ARCH_MAKE)
include $(MAIN_OBJDIR)/check_for_build_mk.mk

VPATH=$(TOPDIR)/Util/Vibra/Src:$(TOPDIR)/Src

FC_DEFAULT:=$(FC)
FC_SERIAL?=$(FC_DEFAULT)
FC:=$(FC_SERIAL)         # Make it non-recursive


# Define these if desired
#
### FPPFLAGS += -DSANKEY_DIAG
### FPPFLAGS += -DVIBRA_DEBUG

# Objects from Siesta
SYSOBJ = $(SYS).o
SIESTA_OBJS = local_sys.o precision.o units.o m_io.o io.o reclat.o $(SYSOBJ)

COMMON_OBJS = recoor.o 
BUILD_OBJS = fcbuild.o $(COMMON_OBJS)
VIBRA_OBJS = vibra.o hermdp.o klines.o outbands.o $(COMMON_OBJS)

INCFLAGS+= $(FDF_INCFLAGS) 

# Build dependencies
$(BUILD_OBJS): $(SIESTA_OBJS)
$(VIBRA_OBJS): $(SIESTA_OBJS)

fcbuild: $(SIESTA_OBJS) $(BUILD_OBJS)
	$(FC) -o fcbuild \
	       $(LDFLAGS) $(BUILD_OBJS) $(SIESTA_OBJS) $(FDF_LIBS)

vibra:  $(COMP_LIBS) $(SIESTA_OBJS) $(VIBRA_OBJS)
	$(FC) -o vibra \
	       $(LDFLAGS) $(VIBRA_OBJS) $(SIESTA_OBJS) $(FDF_LIBS) $(COMP_LIBS) $(LIBS)

clean: 
	@echo "==> Cleaning object and executable files"
	rm -f fcbuild vibra *.o *.mod

install: $(PROGS)
	cp -p $(PROGS) $(SIESTA_INSTALL_DIRECTORY)/bin


# Dependencies
DEP_OBJS= $(SIESTA_OBJS)
LOCAL_OBJS=$(VIBRA_OBJS) $(BUILD_OBJS)

dep:
	sfmakedepend --depend=obj --modext=o \
		$(addprefix $(VPATH)/,$(DEP_OBJS:.o=.f) $(DEP_OBJS:.o=.f90)) \
		$(addprefix $(VPATH)/,$(DEP_OBJS:.o=.F) $(DEP_OBJS:.o=.F90)) \
		$(LOCAL_OBJS:.o=.f90) $(LOCAL_OBJS:.o=.F90) \
		$(LOCAL_OBJS:.o=.f) $(LOCAL_OBJS:.o=.F) \
                || true


# DO NOT DELETE THIS LINE - used by make depend
io.o: m_io.o
units.o: precision.o
fcbuild.o: precision.o units.o
vibra.o: precision.o units.o
