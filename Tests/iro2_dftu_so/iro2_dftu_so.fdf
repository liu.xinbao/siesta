SystemName      (IrO2) Tetragonal structure
#               Tetragonal  phase
#               PBEsol functional 
#               Experimental coordinates taken from 
#               J. Res. Ntl. Bur. Stand. 71, 119, 1967 
#               as cited in the Supplementary Material of
#               Yuan Ping, Giulia Galli, and William A. Goddard, III
#               The Journal of Physical Chemistry C 119, 11570 (2015) 
#               Mesh Cutoff: 200 Ry
#               Monkhorst-Pack grid: 4 x 4 x 3 ; displaced 0.5 0.5 0.5 

SystemLabel	IrO2_dftu_so
NumberOfSpecies 2
NumberOfAtoms	6
%block ChemicalSpeciesLabel
  1 77 Ir_dftu_so
  2 8 O_dftu_so
%endblock ChemicalSpeciesLabel

%block PS.lmax
   Ir_dftu_so    3
    O_dftu_so    3
%endblock PS.lmax
LatticeConstant 1.0 Ang

%block LatticeVectors  
   4.50694600000000   0.00000000000000   0.00000000000000
   0.00000000000000   4.50694600000000   0.00000000000000
   0.00000000000000   0.00000000000000   3.15894500000000
%endblock LatticeVectors

AtomicCoordinatesFormat Fractional
%block AtomicCoordinatesAndAtomicSpecies
   0.00000000000000   0.00000000000000   0.00000000000000  1    
   0.50000000000000   0.50000000000000   0.50000000000000  1    
   0.30811525000000   0.30811525000000   0.00000000000000  2     
   0.69188475000000   0.69188475000000   0.00000000000000  2     
   0.19188475000000   0.80811525000000   0.50000000000000  2     
   0.80811525000000   0.19188475000000   0.50000000000000  2  
%endblock AtomicCoordinatesAndAtomicSpecies

WriteCoorStep           .true.        #  Write the atomic coordinates to 
                                      #     standard output at every 
                                      #     MD time step or relaxation step.

%block kgrid_Monkhorst_Pack
   4  0  0   0.5
   0  4  0   0.5
   0  0  3   0.5
%endblock kgrid_Monkhorst_Pack

#
# DFT, Grid, SCF
#

xc.functional           GGA
xc.authors              PBEsol
MeshCutoff            200 Ry      # Defines the plane wave cutoff for the grid

#
# SCF options
#

SCF.Mix                 Hamiltonian  # (default) Hamiltonian
MaxSCFIterations        300          # (default) 1000
SCF.Mixer.Method        Pulay        # (default) Pulay
SCF.Mixer.Weight        0.05         # (default) 0.25
SCF.Mixer.History       10           # (default) 5
SCF.Mix.Spin            all          # (default) all, Use all spin-components in the mixing
SCF.Mixer.Kick          30           # (default) 0
SCF.Mixer.Kick.Weight   1.0
SCF.DM.Tolerance        1.0E-6       # (default) 1.0E-4
SCF.H.Tolerance         1.0E-5 eV    # (default) 1.0E-3
SCF.MustConverge        .false.
SCF.MixAfterConvergence .false.      # Logical variable to indicate whether mixing
                                     #   is done in the last SCF cycle
                                     #   (after convergence has been achieved)
                                     #   or not.
                                     # Not mixing after convergence improves
                                     #   the quality of the final Kohn-Sham
                                     #   energy and of the forces when mixing
                                     #   the DM.

SolutionMethod          diagon       # (default) diagon
Diag.DivideAndConquer   .false.
ElectronicTemperature   1 K          # Temp. for Fermi smearing, default 300K

DM.UseSaveDM            .false.      # Use DM Continuation files


#
# Conjugate Gradient Relaxation
#

MD.TypeOfRun            cg          # Type of dynamics:
                                    #   - CG
                                    #   - Verlet
                                    #   - Nose
                                    #   - Parrinello-Rahman
                                    #   - Nose-Parrinello-Rahman
                                    #   - Anneal
                                    #   - FC
MD.VariableCell         .true.      # The lattice is relaxed together with
                                    # the atomic coordinates?
MD.NumCGsteps             0         # Number of CG steps for
                                    #   coordinate optimization
MD.MaxCGDispl           0.05 Bohr   # Maximum atomic displacement
                                    #   in one CG step
MD.MaxForceTol         0.01 eV/Ang  # Tolerance in the maximum
                                    #   atomic force
MD.MaxStressTol        0.0001 eV/Ang**3  
                                    # Tolerance in the maximum
                                    #   stress in a MD.VariableCell CG optimi.

MD.UseSaveXV           .false.      # Instructs Siesta to read the 
                                    #   atomic positions and velocities stored
                                    #   in file SystemLabel.XV by a 
                                    #   previous run.
MD.UseSaveCG           .false.      # Instructs to read the conjugate-gradient 
                                    #   hystory information stored in file 
                                    #   SystemLabel.CG by a previous run.

%block PAO.Basis                 # Define Basis set
Ir_dftu_so   2     
n=6   0   1   E   100.00000     4.00000
     6.00000 
     1.00000 
n=5   2   1   E   100.00000     3.50000
     4.50000 
     1.00000 
O_dftu_so    2    
n=2   0   1   E   100.00000     4.00000
     6.00000
     1.00000
n=2   1   1   E   100.00000     4.00000
     5.50000
     1.00000
%endblock PAO.Basis

#
# Spin Magnetism
#

Spin spin-orbit
Spin.OrbitStrength  1.0

# To initialize the atomic spin along x, uncomment these lines
#%block DM.InitSpin
# 1  +3.  90.   0.      # Atom index, spin, theta, phi (deg)
# 2  -3.  90.   0.      # Atom index, spin, theta, phi (deg)
#%endblock DM.InitSpin

# To initialize the atomic spin along y, uncomment these lines
#%block DM.InitSpin
# 1  +3.  90.  90.      # Atom index, spin, theta, phi (deg)
# 2  -3.  90.  90.      # Atom index, spin, theta, phi (deg)
#%endblock DM.InitSpin

# To initialize the atomic spin along z, uncomment these lines
%block DM.InitSpin
 1  +3.   0.   0.      # Atom index, spin, theta, phi (deg)
 2  -3.   0.   0.      # Atom index, spin, theta, phi (deg)
%endblock DM.InitSpin

#
# LDAU
#

LDAU.ProjectorGenerationMethod      1
%block LDAU.proj
Ir_dftu_so   1        # number of shells of projectors
n=5 2         # n, l
2.000 0.500   # U(eV), J(eV)
3.000         # rc, \omega (default values)
%endblock LDAU.proj

WriteCoorStep          .true.
WriteForces             T
WriteIonPlotFiles .true.
WriteMullikenPop       1

